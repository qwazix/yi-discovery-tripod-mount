# YI Discovery case with tripod mount

A simple case for YI-Discovery with a 1/4 tripod screw thread.

It has an opening for the usb cable. The repository also includes a mount 
for 2020 aluminium profile so you can use standard ball flexible arms
such as [this](https://joby.com/suction-cup-with-gorillapod-arm)

Thanks to Dan Kirshner for the thread library.


## This project is no longer maintained.

The discovery case has moved along with other brackets and mounts to [various-tripod-mounts](https://gitlab.com/qwazix/various-tripod-mounts)