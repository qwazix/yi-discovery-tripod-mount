/*
 * 20x20 aluminium profile tripod/arm mount
 *
 * Copyright 2018 Michael Demetriou - qwazix@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See <http://www.gnu.org/licenses/>.
 */

include <threads.scad>

mount_size = [40,20,9];
mount_bolts = 3.6;
countersunk = true;

//mount_size = [80,30,12];
//mount_bolts = 9;
//countersunk = false;

mirror([0,0,1]) difference(){
    cube(mount_size);
    translate([mount_size[0]/2,mount_size[1]/2,-0.1]) english_thread (diameter=1/4+0.025, threads_per_inch=20, length=0.2);
    translate([2*mount_bolts,mount_size[1]/2,-0.1]) mount_bolt();
    translate([mount_size[0]-(2*mount_bolts),mount_size[1]/2,-0.1]) mount_bolt();
}

module mount_bolt(){
    cylinder(d=mount_bolts, h=mount_size[2]+3, $fn=32);
    if (countersunk) cylinder(d=mount_bolts*2, h=mount_bolts, $fn=32);
}