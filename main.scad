/*
 * yi discovery tripod mount/case
 *
 * Copyright 2018 Michael Demetriou - qwazix@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See <http://www.gnu.org/licenses/>.
 */

include <threads.scad>

camera_size = [64,22,43];

case();

module case(smile=true,tripod=true){
    difference(){
        cube(camera_size+[4,4,0]);
        translate([1.5,2,6]) camera();
        if (tripod) translate([camera_size[0]/2,camera_size[1]/2,-0.1]) english_thread (diameter=1/4, threads_per_inch=20, length=0.2);
        if (smile) rotate([90,0,0]) linear_extrude(height = 1, center = true, convexity = 10) text("smile :-)", size=11);
    }
}

module camera(){
    diff = 1;
    
    //camera lens
    lens_d = 22;
    translate([50,3,22]) rotate([90,0,0]) {
        hull(){
            translate([0,30,0]) cube(lens_d,true);
            cylinder(d=lens_d, h=20);
        }
    }
    
    //lcd space
    translate([2,10,2]) cube(camera_size-[4,4,0]);
    
    intersection(){ 
        translate(camera_size/2-[-diff/2,-camera_size[1]/2,0]) rotate([90,0,0]) scale([1,4,1]) cylinder(d=camera_size[0]+diff,h=camera_size[1],$fn=128);
        translate([0,0,0]) cube(camera_size+[diff,0,0]);
    }
    //usb port
    translate([-9,10,23]) cube([10,8,10]);
    //#translate([diff/2,0,0]) cube(camera_size);
}


//translate([2,-1,10]) camera();