/* This file is in the public domain */

mirror([0,0,1]) difference(){
    cylinder(d=21.5, h=8);
    cylinder(d=20.5, h=7);
}